'''Desarrollar una clase que represente un Cuadrado
y tenga los siguientes métodos: inicializar el valor del lado
llegando como parámetro al método __init__ (definir un atributo llamado lado),
imprimir su perímetro y su superficie. '''

class Cuadrado():

    def __init__(self, lado):
        self.lado=lado

    def perimetro_imp(self):
        print("El perimetro es: ", self.lado*4)

    def superficie_imp(self):
        print("La superficie es: ", self.lado*self.lado)

#bloque principal

cuadrado1=Cuadrado(5)
cuadrado1.perimetro_imp()
cuadrado1.superficie_imp()

