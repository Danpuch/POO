'''Implementar una clase llamada Alumno que tenga como atributos 
su nombre y su nota. Definir los métodos para inicializar sus atributos, 
imprimirlos y mostrar un mensaje si está regular (nota mayor o igual a 4)

Definir dos objetos de la clase Alumno. 43 '''

class Alumno():

    def inicializar(self, nombre, nota):
        self.nombre=nombre
        self.nota=nota

    def imprimir(self):
        print("El alumno", self.nombre,"tiene una nota de", self.nota)

    def notamayor(self):
        if self.nota>5:
            print("Enhorabuena, tu nota es mayor a 5")
        else:
            print("No superas el 5")

#bloque principal

nombre1=input("Escribe nombre de alumno 1: ")
alumno1=Alumno()
alumno1.inicializar(nombre1, 4)
alumno1.imprimir()
alumno1.notamayor()

nombre2=input("Escribe nombre de alumno 2: ")
alumno2=Alumno()
alumno2.inicializar(nombre2, 6)
alumno2.imprimir()
alumno2.notamayor()