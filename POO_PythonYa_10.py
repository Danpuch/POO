'''Implementar la clase Operaciones. Se deben cargar dos valores enteros
por teclado en el método __init__, calcular su suma, resta, multiplicación
y división, cada una en un método, imprimir dichos resultados'''

class Operaciones():

    def __init__(self):
        self.valor1=float(input("Ingrese valor: "))
        self.valor2=float(input("Ingrese otro valor: "))

    def sumar(self):
        sumsum= self.valor1 + self.valor2
        print("La suma de los dos numeros es: ", sumsum)

    def restar(self):
        restaresta=self.valor1 - self.valor2
        print("La resta de los dos numeros es: ", restaresta)

    def multiplica(self):
        multi=self.valor1 * self.valor2
        print("La multiplicacion es: ", multi)

    def divide(self):
        divi=self.valor1 / self.valor2
        print("La division es: ", divi)

#bloque principal

operando1=Operaciones()
operando1.sumar()
operando1.restar()
operando1.multiplica()
operando1.divide()
print("EL valor introducido es: ", operando1.valor1, operando1.valor2)