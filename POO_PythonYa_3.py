'''Implementar una clase llamada Alumno que tenga como atributos 
su nombre y su nota. Definir los métodos para inicializar sus atributos, 
imprimirlos y mostrar un mensaje si está regular (nota mayor o igual a 4)

Definir dos objetos de la clase Alumno. 43 '''

class Alumno():

    def inicializacion(self, nombre, nota):
        self.nombre = nombre
        self.nota = nota

    def imprimir(self):
        print("El alumno", self.nombre, "tiene una nota de", self.nota)

    def mayorde(self):
        if self.nota>5:
            print("Enhorabuena, tu nota es superior a 5")
        else:
            print("Lo siento, no superas el 5")

#bloque principal

nombre1=input("Ingresa nombre de alumno 1:")
nota1=int(input("Nota:"))
nombre2=input("Ingresa nombre de alumno 2:")
nota2=int(input("Nota:"))
alumno1=Alumno()
alumno1.inicializacion(nombre1, nota1)
alumno1.imprimir()
alumno1.mayorde()

alumno2=Alumno()
alumno2.inicializacion(nombre2, nota2)
alumno2.imprimir()
alumno2.mayorde()