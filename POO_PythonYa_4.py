'''Confeccionar una clase que permita carga el nombre y la edad
de una persona. Mostrar los datos cargados.
Imprimir un mensaje si es mayor de edad (edad>=18) '''

class Persona():

    def carga_datos(self, nombre, edad):
        self.nombre=nombre
        self.edad=edad

    def imprimir(self):
        print("La persona", self.nombre, "tiene una edad de", self.edad)

    def mayorde(self):
        if self.edad>=18:
            print("La persona", self.nombre,"es mayor de edad")
        else:
            print("La persona", self.nombre,"NO es mayor de edad")


nombre1=input("Ingresa nombre:")
edad1=int(input("Edad:"))
nombre2=input("Ingresa nombre de la segunda persona:")
edad2=int(input("Edad:"))

persona1=Persona()
persona1.carga_datos(nombre1, edad1)
persona1.imprimir()
persona1.mayorde()

persona2=Persona()
persona2.carga_datos(nombre2, edad2)
persona2.imprimir()
persona2.mayorde()