'''Plantear una clase Operaciones que solicite en el método __init__
la carga de dos enteros e inmediatamente muestre su suma,
resta, multiplicación y división. Hacer cada operación en
otro método de la clase Operación y llamarlos desde el mismo método __init__'''

class Operaciones():

    def __init__(self):
        self.valor1=int(input("Ingrese primer valor: "))
        self.valor2=int(input("Ingrese segundo valor: "))
        self.sumar()
        self.restar()
        self.multiplica()
        self.divide()

    def sumar(self):
        sumandos=self.valor1 + self.valor2
        print("La suma es", sumandos)

    def restar(self):
        resta=self.valor1 - self.valor2
        print("La resta es", resta)

    def multiplica(self):
        multi=self.valor1*self.valor2
        print("La multiplicacion es", multi)

    def divide(self):
        divi=self.valor1 / self.valor2
        print("La division es", divi)

#bloque principal

operacion1=Operaciones()